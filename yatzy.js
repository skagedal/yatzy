var YatzyMove = {
    ONES: 1,
    TWOS: 2,
    THREES: 3,
    FOURS: 4,
    FIVES: 5,
    SIXES: 6,
    ONE_PAIR: 7,
    TWO_PAIRS: 8,
    THREE_OF_A_KIND: 9,
    FOUR_OF_A_KIND: 10,
    SMALL_STRAIGHT: 11,
    LARGE_STRAIGHT: 12,
    FULL_HOUSE: 13,
    CHANCE: 14,
    YATZY: 15
};

function emptyScoreTable() {
    var scoreTable = {};
    var i;
    for (i = YatzyMove.ONES; i <= YatzyMove.YATZY; i++)
        scoreTable[i] = null;
    return scoreTable;
}


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function rollDice(kept) {
    var newRoll = [], i;
    for (i = 0; i < 5; i++) {
        if (i < kept.length)
            newRoll[i] = kept[i];
        else
            newRoll[i] = getRandomInt(1, 6);
    }
    return newRoll;
}

function runGame(playFunc) {
    var scoreTable = emptyScoreTable();
    var moves = 0;
    var playerState = {};

    // Initialize player                                                        
    playFunc(playerState, [], 0);

    for (moves = 0; moves < 15; moves++) {
        var roll, dice = [], ret;
  	    for (roll = 1; roll <= 3; roll++) {
            dice = rollDice(dice);
            ret = playFunc(playerState, dice, roll);

        }
    }
}


